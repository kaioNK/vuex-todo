import axios from 'axios'

const state = {
   todos: []
}

const getters = {
   allTodos: state => state.todos
}

const actions = {
   async fetchTodos({commit}){
      const response = await axios.get('https://jsonplaceholder.typicode.com/todos')

      console.log(response.data)
      commit('SET_TODOS', response.data)
   },

   async addTodos({commit}, title){
      const response = await axios.post('https://jsonplaceholder.typicode.com/todos', {title, completed: false})

      commit('NEW_TODO', response.data)
   },

   async deleteTodo({commit}, id){
      const response = await axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)

      commit('REMOVE_TODO', id)
   },

   async filterTodo({commit}, e){
      console.log(e)

      //Get selected number
      const limit = parseInt(
         e.target.options[e.target.options.selectedIndex].innerText
      )
      console.log(limit)

      const response = await axios.get(`https://jsonplaceholder.typicode.com/todos?_limit=${limit}`)

      commit('SET_TODOS', response.data)
   },

   async updateTodo({commit}, upTodo){
      const response = await axios.put(`https://jsonplaceholder.typicode.com/todos/${upTodo.id}`, upTodo)

      console.log(response.data)
      commit('UPTODO', response.data)
   }
}

const mutations = {
   SET_TODOS: (state, todos) => (state.todos = todos),
   NEW_TODO: (state, todo) => state.todos.unshift(todo),
   REMOVE_TODO: (state, id) => state.todos = state.todos.filter(todo => todo.id !== id),
   UPTODO: (state, upTodo) => {
      const index = state.todos.findIndex(todo => todo.id === upTodo.id)

      if(index !== -1){
         state.todos.splice(index, 1, upTodo)
      }
   }
}

export default{
   state, getters, actions, mutations
}